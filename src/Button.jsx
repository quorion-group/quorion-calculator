import React from "react";

function Button({ value, onClick, className }) {
  return (
    <div className={"btn number " + className} onClick={() => onClick(value)}>
      {value}
    </div>
  );
}

export default Button;
