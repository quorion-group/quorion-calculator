import React, { useState } from "react";
import "./App.css";
import Button from "./Button";

function App() {
  const [result, setResult] = useState(0);
  const [temp, setTemp] = useState("");
  const [preFunction, setPreFunction] = useState("");
  const [isCalculateResult, setIsCalculateResult] = useState(false);
  const [value, setValue] = useState("");
  const [history, setHistory] = useState([]);
  const [formula, setFormula] = useState("");
  const [isEqualFunction, setIsEqualFunction] = useState(false);

  const calculate = (functionKey) => (a, b) => {
    const valueA = parseFloat(a);
    const valueB = parseFloat(b);
    switch (functionKey) {
      case "+":
        return valueA + valueB;
      case "-":
        return valueA - valueB;
      case "x":
        return valueA * valueB;
      case "/":
        return valueB === 0 ? 0 : valueA / valueB;
      default:
        return valueB;
    }
  };

  function reset() {
    setFormula("");
    setPreFunction("");
    setValue("");
    setTemp("");
    setResult(0);
    setIsCalculateResult(false);
  }

  function handleClick(key) {
    if (key === "CE") {
      if (value.length > 0) {
        setValue((prev) => prev.substring(0, prev.length - 1));
        setFormula((prev) => prev.substring(0, prev.length - 1));
      }
      return;
    }

    if (key === "C") {
      reset();
      return;
    }

    if (["/", "+", "-", "x", "="].includes(key)) {
      if (key !== "=") {
        setIsEqualFunction(false);
        setPreFunction(key);
      } else setIsEqualFunction(true);

      if (key !== "=" && isCalculateResult) {
        setFormula((prev) => `${prev.split(" ")[0]} ${key} `);
        return;
      }

      const valueToCalculate = isCalculateResult ? temp : value;

      const currentResult = calculate(preFunction)(result, valueToCalculate);
      setResult(currentResult);

      setTemp(valueToCalculate);

      if (preFunction) setValue(currentResult);

      if (!isCalculateResult) {
        if (preFunction) {
          const formulaHistory = `${formula} = ${currentResult}`;
          history.push(formulaHistory);
          setHistory(history);
        }

        if (key === "=") setFormula(`${currentResult}`);
        else setFormula(`${currentResult} ${key} `);
      } else setFormula((prev) => ` ${prev} ${key} `);

      setIsCalculateResult(true);

      return;
    }

    if (value.length > 15) return;

    if (key === "." && value.indexOf(".") > -1) return;

    if (isEqualFunction) {
      reset();
      setIsEqualFunction(false);
    }

    setValue((prev) => {
      if (isCalculateResult) {
        setIsCalculateResult(false);
        return key;
      } else return prev + key;
    });

    setFormula((prev) => prev + key);
  }

  return (
    <div className="container">
      <Button
        value="Clear History"
        className="clear"
        onClick={() => setHistory([])}
      />
      <table>
        <tbody>
          <tr>
            <td colSpan="4">
              <div className="history info">
                {history.map((h, index) => (
                  <div key={index}>{h}</div>
                ))}
              </div>
              <div type="text" className="input">
                <div className="formula">{formula}</div>
                <div>{value}</div>
              </div>
            </td>
          </tr>
          <tr>
            <td colSpan="2">
              <Button value="CE" className="function" onClick={handleClick} />
            </td>
            <td colSpan="2">
              <Button value="C" className="function" onClick={handleClick} />
            </td>
          </tr>
          <tr>
            <td>
              <Button value="7" onClick={handleClick} />
            </td>
            <td>
              <Button value="8" onClick={handleClick} />
            </td>
            <td>
              <Button value="9" onClick={handleClick} />
            </td>
            <td>
              <Button value="/" className="function" onClick={handleClick} />
            </td>
          </tr>
          <tr>
            <td>
              <Button value="4" onClick={handleClick}></Button>
            </td>
            <td>
              <Button value="5" onClick={handleClick}></Button>
            </td>
            <td>
              <Button value="6" onClick={handleClick}></Button>
            </td>
            <td>
              <Button
                value="x"
                className="function"
                onClick={handleClick}
              ></Button>
            </td>
          </tr>
          <tr>
            <td>
              <Button value="1" onClick={handleClick}></Button>
            </td>
            <td>
              <Button value="2" onClick={handleClick}></Button>
            </td>
            <td>
              <Button value="3" onClick={handleClick}></Button>
            </td>
            <td>
              <Button
                value="-"
                className="function"
                onClick={handleClick}
              ></Button>
            </td>
          </tr>
          <tr>
            <td>
              <Button value="0" onClick={handleClick}></Button>
            </td>
            <td>
              <Button value="." onClick={handleClick}></Button>
            </td>
            <td>
              <Button
                value="="
                className="function"
                onClick={handleClick}
              ></Button>
            </td>
            <td>
              <Button
                value="+"
                className="function"
                onClick={handleClick}
              ></Button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
