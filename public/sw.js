const staticCacheName = "static-cache-v4";

const assets = [
  "/",
  "/index.html",
  "/favicon.ico",
  "/logo128.png",
  "/logo256.png",
  "/logo512.png",
  "/logo1024.png",
  "/manifest.json",
  "/static/js/bundle.js",
  "/static/js/vendors~main.chunk.js",
  "/static/js/main.chunk.js",
];

const self = this;
self.addEventListener("install", (event) => {
  console.log("install...");
  event.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      return cache.addAll(assets);
    })
  );
});

self.addEventListener("activate", (event) => {
  console.log("activate...");
  event.waitUntil(
    caches.keys().then((keys) => {
      return Promise.all(
        keys
          .filter((key) => key !== staticCacheName)
          .map((key) => caches.delete(key))
      );
    })
  );
});

self.addEventListener("fetch", (event) => {
  event.respondWith(
    caches
      .match(event.request)
      .then((cacheRes) => cacheRes || fetch(event.request))
  );
});
